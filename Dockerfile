FROM nikolaik/python-nodejs:python3.8-nodejs14


ENV PYTHONUNBUFFERED 1

RUN mkdir /usr/src/app
COPY . /usr/src/app

WORKDIR /usr/src/app

RUN pip install -r requirements.txt

RUN apt-get update
RUN apt-get install -y apt-utils
RUN apt-get install -y gettext libgettextpo-dev

RUN pip install psycopg2-binary

RUN npm install -g mjml

ARG BLOCKTINU_BACKEND_ENV="development"

RUN echo blocktinu_backend Dockerfile: $BLOCKTINU_BACKEND_ENV env

ARG PORT=9007

EXPOSE ${PORT}
EXPOSE 443

CMD ./run.sh

