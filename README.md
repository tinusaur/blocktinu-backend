# Blocktinu Backend

#### API endpoints description

Use [Insomnia](https://insomnia.rest) and `./Insomnia_yyyy-mm-dd.json` file to explore major API endpoints.

#### Token verification

See `./Insomnia-{date}.json` file, the description of the most API endpoints provided by this project.

```bash
▲ ~ curl -X GET http://localhost:9442/api/auth/users/me/ -H "Authorization: Token 822b0ffb7f2853eaa432e55714055678af6614e8"
```
The response should be either `401 Unauthorized` (invalid token) or `200 OK` (valid token) with the following JSON response:
```json
{
    "role":2,
    "first_name":"Vlad",
    "last_name":"...",
    "email":"...@gmail.com",
    "is_active":true,
    "id":2,
    "url":"http://localhost:9442/api/users/2/",
    "last_login":"2019-10-04T12:28:34.483024Z",
    "is_online":null,
    "classrooms_count":0
}
```
