import os
import django

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "blocktinu_backend.settings")
django.setup()

from channels.auth import AuthMiddlewareStack

from channels.routing import ProtocolTypeRouter, URLRouter
from django.conf.urls import url

from blocktinu_backend.socket_connection.consumers import GlobalConsumer
from blocktinu_backend.socket_connection.middleware import TokenAuthMiddleware

# Middleware stack for socket authentication using token
def TokenAuthMiddlewareStack(inner):
    return TokenAuthMiddleware(AuthMiddlewareStack(inner))


websocket_urlpatterns = [
    url(r'^blocktinu_ws/global/$', GlobalConsumer),
]

application = ProtocolTypeRouter({
    # (http->django views is added by default)
    'websocket': TokenAuthMiddlewareStack(
        URLRouter(
            websocket_urlpatterns
        )
    ),
})
