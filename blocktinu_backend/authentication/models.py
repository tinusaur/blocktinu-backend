from django.db import models
from django.contrib.auth.models import AbstractUser, UserManager

from django.utils import timezone
from django.core.cache import cache


class CustomUserManager(UserManager):

    def _create_user(self, email, password, **extra_fields):
        """
        Create and save a user with the given username, email, and password.
        """
        if not email:
            raise ValueError('The given email must be set')
        email = self.normalize_email(email)
        user = self.model(email=email, **extra_fields)
        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_user(self, email=None, password=None, **extra_fields):
        extra_fields.setdefault('is_staff', False)
        extra_fields.setdefault('is_superuser', False)
        return self._create_user(email, password, **extra_fields)

    def create_superuser(self, email=None, password=None, **extra_fields):
        extra_fields.setdefault('is_staff', True)
        extra_fields.setdefault('is_superuser', True)

        if extra_fields.get('is_staff') is not True:
            raise ValueError('Superuser must have is_staff=True.')
        if extra_fields.get('is_superuser') is not True:
            raise ValueError('Superuser must have is_superuser=True.')

        return self._create_user(email, password, **extra_fields)

class User(AbstractUser):
    STAFF = 1
    TEACHER = 2
    STUDENT = 3
    ROLE_CHOICES = (
        (STAFF, "Staff"),
        (TEACHER, "Teacher"),
        (STUDENT, "Student"),
    )
    role = models.PositiveSmallIntegerField(choices=ROLE_CHOICES, default=STUDENT)

    username = None
    objects = CustomUserManager()

    email = models.EmailField("email address", unique=True)
    REQUIRED_FIELDS = ["role", "first_name", "last_name"]
    EMAIL_FIELD = "email"
    USERNAME_FIELD = "email"

    # classrooms (classrooms)
    # last_active = models.DateTimeField(default=timezone.now)

    def get_online_status_cache_key(self):
        return 'ONLINE_CHANNELS_COUNT_USER_{}'.format(self.id)

    def set_online_status(self, is_online):
        key = self.get_online_status_cache_key()
        count = cache.get(key)
        difference = 1 if is_online else -1

        if count is None:
            count = 0

        new_count = count + difference

        if new_count == 0:
            cache.delete(key)
            return None

        cache.set(key, new_count)
        return new_count

    @property
    def is_online(self):
        cache_value = cache.get(self.get_online_status_cache_key())
        return cache_value

    @property
    def full_name(self):
        return "{} {}".format(self.first_name, self.last_name)