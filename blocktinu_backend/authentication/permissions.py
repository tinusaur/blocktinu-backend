from rest_framework import permissions

from .models import User


class IsOwner(permissions.BasePermission):
    """Is owner of classroom"""
    def has_permission(self, request, view):
        return True

    def has_object_permission(self, request, view, obj):
        if isinstance(obj, User):
            return obj == request.user or request.user.role == User.STAFF
        # no.
        return False


class IsOwnerOrReadOnly(IsOwner):
    """Is classroom owner or read only"""
    def has_object_permission(self, request, view, obj):
        if request.method in permissions.SAFE_METHODS:
            return True

        return super(IsOwnerOrReadOnly, self).has_object_permission(request, view, obj)

