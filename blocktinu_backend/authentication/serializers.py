from django.contrib.auth.models import Group
from django.contrib.auth import get_user_model
from rest_framework import serializers
# from djoser.serializers import UserSerializer as BaseUserSerializer, UserCreateSerializer as BaseUserRegistrationSerializer

# from djoser.compat import get_user_email, get_user_email_field_name
from djoser.conf import settings

User = get_user_model()


class UserSerializer(serializers.HyperlinkedModelSerializer):
    first_name = serializers.CharField(required=True)

    classrooms_count = serializers.SerializerMethodField()

    def get_classrooms_count(self, obj):
        return obj.classrooms.count()

    class Meta:
        model = User
        fields = tuple(User.REQUIRED_FIELDS) + (
            # settings.USER_ID_FIELD,
            settings.LOGIN_FIELD,
            'first_name', 'last_name', 'is_active', 'role', 'id', 'url', 'last_login', 'is_online', 'classrooms_count'
        )
        read_only_fields = (settings.LOGIN_FIELD,)
        required_fields = ('email', 'role', 'first_name', 'last_name', )
        extra_kwargs = {field: {'required': True} for field in required_fields}


class PublicUserSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = User
        fields = (
            'id', 'url', 'first_name', 'last_name', 'role', 'is_active', 'last_login', 'is_online'
        )


#     def update(self, instance, validated_data):
#         email_field = get_user_email_field_name(User)
#         if settings.SEND_ACTIVATION_EMAIL and email_field in validated_data:
#             instance_email = get_user_email(instance)
#             if instance_email != validated_data[email_field]:
#                 instance.is_active = False
#                 instance.save(update_fields=["is_active"])
#         return super().update(instance, validated_data)


# class UserRegistrationSerializer(BaseUserRegistrationSerializer):
#     class Meta(BaseUserRegistrationSerializer.Meta):
#         model = User
#         fields = ('url', 'email', 'role', 'first_name', 'last_name')
#         required_fields = ('email', 'role', 'first_name', 'last_name')
#         extra_kwargs = {field: {'required': True} for field in required_fields}


class GroupSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Group
        fields = ['url', 'name']
