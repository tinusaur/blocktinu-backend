from rest_framework.response import Response
from rest_framework.authtoken.models import Token

from django.utils.six import text_type


class TokenStrategy:

    @classmethod
    def obtain(cls, user):
        token, created = Token.objects.get_or_create(user=user)
        print(token.key)
        return {
            "access": text_type(token.key),
            "refresh": text_type(token.key),
            "user": user,
        }
#         return {
#             "access":
#             "token": token.key,
#         }


# class TokenStrategy:
#     @classmethod
#     def obtain(cls, user):
#         from rest_framework_simplejwt.tokens import RefreshToken
#         from django.utils.six import text_type

#         refresh = RefreshToken.for_user(user)
#         return {
#             "access": text_type(refresh.access_token),
#             "refresh": text_type(refresh),
#             "user": user,
#         }
