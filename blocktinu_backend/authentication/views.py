"""
Views auth & users
"""
from rest_framework import viewsets, filters
# from rest_framework.decorators import action
# from rest_framework.response import Response

from django_filters import rest_framework as django_rest_filters
from .permissions import IsOwnerOrReadOnly
from .models import User
from .serializers import UserSerializer, PublicUserSerializer

class UserViewSet(viewsets.ModelViewSet):
    """
    Users view set
    """
    queryset = User.objects.order_by('-date_joined')
    serializer_class = PublicUserSerializer
    permission_classrooms = (IsOwnerOrReadOnly, )
    filter_backends = (filters.SearchFilter,
                       django_rest_filters.DjangoFilterBackend)
    search_fields = ('first_name', 'last_name', '=email', )
    filter_fields = ('classrooms__id', )



    def get_serializer(self, *args, **kwargs):
        """
        Return the serializer instance that should be used for validating and
        deserializing input, and for serializing output.
        """
        context = self.get_serializer_context()
        kwargs['context'] = context
        if isinstance(args[0], list):
            serializer_class = self.get_serializer_class()
        if len(args) > 0 and isinstance(args[0], User) and context['request'].user and context['request'].user.id == args[0].id:
            serializer_class = UserSerializer
        else:
            serializer_class = self.get_serializer_class()
        return serializer_class(*args, **kwargs)