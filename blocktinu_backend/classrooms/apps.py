from django.apps import AppConfig


class ClassRoomsConfig(AppConfig):
    name = 'classrooms'
