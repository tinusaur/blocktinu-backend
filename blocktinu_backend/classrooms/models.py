from django.db import models
from django.contrib.contenttypes.fields import GenericRelation
from django.contrib.auth import get_user_model
from blocktinu_backend.images.models import Image

from django.db.models.signals import m2m_changed
from django.dispatch import receiver

from blocktinu_backend.notifications.models import Notification

User = get_user_model()

class ClassRoom(models.Model):
    owner = models.ForeignKey(User, on_delete=models.SET_NULL, null=True)
    members = models.ManyToManyField(User, related_name="classrooms")
    title = models.TextField(max_length=100, blank=False, null=False)
    description = models.TextField(max_length=900, blank=True, null=False)

    finished = models.BooleanField(default=False)

    images = GenericRelation(Image)

    # ?
    # shield = models.CharField(max_length=50, blank=False, null=False)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)


@receiver(m2m_changed, sender=ClassRoom.members.through)
def classroom_members_changes(sender, instance, action, reverse, model, pk_set, **kwargs):
    # create notification for new classroom members
    # see https://docs.djangoproject.com/en/dev/ref/signals/#m2m-changed
    if action == "post_add":
        new_members = instance.members.filter(pk__in=pk_set)
        for member in new_members:
            title = "You have been added to a classroom"
            text = "{} added you to a classroom {}".format(
                instance.owner.first_name, instance.title)
            redirect_path = "classrooms/details/{}".format(instance.id)
            try:
                Notification.objects.create(owner=instance.owner,
                    recipient=member, title=title, text=text, redirect_path=redirect_path)
            except:
                continue
