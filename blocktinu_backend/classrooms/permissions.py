from rest_framework import permissions

from .models import ClassRoom


class IsOwner(permissions.BasePermission):
    """Is owner of classroom"""
    def has_permission(self, request, view):
        return True

    def has_object_permission(self, request, view, obj):

        if isinstance(obj, ClassRoom):
            return obj.owner == request.user
        # no.
        return False


class IsOwnerOrReadOnly(IsOwner):
    """Is classroom owner or read only"""
    def has_object_permission(self, request, view, obj):
        if request.method in permissions.SAFE_METHODS:
            return True

        return super(IsOwnerOrReadOnly, self).has_object_permission(request, view, obj)

