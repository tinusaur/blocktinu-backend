from rest_framework import serializers
from .models import ClassRoom
from blocktinu_backend.images.models import Image
from blocktinu_backend.images.serializers import ImageSerializer

from django.core.exceptions import ObjectDoesNotExist
from django.utils.encoding import smart_text

from blocktinu_backend.authentication.serializers import UserSerializer


class ClassRoomSerializer(serializers.HyperlinkedModelSerializer):
    """ClassRoom serializer"""

    images = ImageSerializer(
        many=True,
        read_only=True)

    class Meta:
        model = ClassRoom
        fields = ("id", "url", "owner", "owner_id", "members", "title", "description",
                  "images", "finished", "created_at", "updated_at", )
        read_only_fields = ("id", "url", "owner", "images",
                            "created_at", "updated_at", )
        required_fields = ("title", "description", "members")
        extra_kwargs = {field: {"allow_null": False, "required": True}
                        for field in required_fields}

    def get_current_user(self):
        """Gets Current user from request"""
        user = None
        request = self.context.get("request")
        if request and hasattr(request, "user"):
            user = request.user
            return user
        return None

    def validate_owner(self, value):
        """Validate owner field"""
        user = self.get_current_user()
        if user.is_anonymous or user != value:
            raise serializers.ValidationError(
                "You can not create a classroom for another user")
        return value

    def validate_members(self, value):
        """Validate members field"""
        request = self.context.get("request")
        user = self.get_current_user()

        if not user:
            raise serializers.ValidationError("Login please")

        if len(value) < 2:
            raise serializers.ValidationError("At least 2 users required")

        members = value

        request_user_is_in_members = False
        for u in members:
            if u.id == user.id:
                request_user_is_in_members = True

        if not request_user_is_in_members:
            raise serializers.ValidationError({"detail":
                                               "Members field must contain request user"})

        return value

    def create(self, validated_data):
        """Add image objects"""
        """Check count of image objects to create (allowed 1)"""
        image_data = self.context.get("view").request.FILES
        if len(image_data) > 1:
            raise serializers.ValidationError(
                {"detail": "You can not update more than one image!"})
        project_data = super().create(validated_data)
        for img in image_data.values():
            img = Image.objects.create(content_object=project_data, src=img)
        return project_data

    def update(self, instance, validated_data):
        """Add image objects while companie object is on update"""
        """Check count of image objects to create (allowed 1)"""
        image_data = self.context.get("view").request.FILES
        if len(image_data) > 1:
            raise serializers.ValidationError(
                {"detail": "You can not update more than one image!"})
        # update
        project_data = super().update(instance, validated_data)
        # remove images
        if len(image_data) == 1:
            images_to_replace = Image.objects.filter(object_id=instance.id)
            images_to_replace.delete()

            for img in image_data.values():
                img = Image.objects.create(
                    content_object=project_data, src=img)
        return project_data

    def to_representation(self, instance):
        """Add serialized data to response"""
        response = super().to_representation(instance)
        user = self.get_current_user()
        if self.context["request"] and user:
            response['owner'] = UserSerializer(
                instance.owner, many=False, context=self.context).data
            if not isinstance(self.instance, list):
                # members = instance.members.exclude(id=user.id)
                response['members'] = UserSerializer(
                    instance.members, many=True, context=self.context).data
        return response
