"""
Projects urls
"""

from rest_framework import routers
from django.conf.urls import url, include
from . import views

ROUTER = routers.DefaultRouter()
ROUTER.register(r'classrooms', views.ClassRoomViewSet, base_name="classroom")

urlpatterns = [
    url(r'', include(ROUTER.urls))
]

