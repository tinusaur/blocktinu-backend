import os

# Django app secret key
SECRET_KEY = os.environ.get('DJANGO_SECRET_KEY')

#
# DB
#
POSTGRES_PASSWORD = os.environ.get('POSTGRES_PASSWORD')
# set in docker-compose.yml (internal container)
POSTGRES_HOST = os.environ.get('POSTGRES_HOST')
POSTGRES_PORT = os.environ.get('POSTGRES_PORT')

# Redis for django channels
REDIS_HOST = os.environ.get('REDIS_HOST')
REDIS_PORT = os.environ.get('REDIS_PORT')

# Memcached for cache
MEMCACHED_LOCATION = os.environ.get('MEMCACHED_LOCATION')

# django public access
BACKEND_PUBLIC_PORT = os.environ.get('BACKEND_PUBLIC_PORT')
BACKEND_PUBLIC_HOST = os.environ.get('BACKEND_PUBLIC_HOST')
BACKEND_PUBLIC_URL = os.environ.get('BACKEND_PUBLIC_URL')

# Email
EMAIL_HOST = os.environ.get('EMAIL_HOST')
EMAIL_PORT = os.environ.get('EMAIL_PORT', 587)
EMAIL_HOST_USER = os.environ.get('EMAIL_HOST_USER')
EMAIL_HOST_PASSWORD = os.environ.get('EMAIL_HOST_PASSWORD')

# Social keys
GOOGLE_KEY = os.environ.get('GOOGLE_KEY')
GOOGLE_SECRET = os.environ.get('GOOGLE_SECRET')

FACEBOOK_KEY = os.environ.get('FACEBOOK_KEY')
FACEBOOK_SECRET = os.environ.get('FACEBOOK_SECRET')

WEBCLIENT_PUBLIC_PROTOCOL = os.environ.get('WEBCLIENT_PUBLIC_PROTOCOL')
WEBCLIENT_PUBLIC_HOST = os.environ.get('WEBCLIENT_PUBLIC_HOST')
WEBCLIENT_PUBLIC_PORT = os.environ.get('WEBCLIENT_PUBLIC_PORT')
WEBCLIENT_PUBLIC_HOST_WITH_PORT = os.environ.get('WEBCLIENT_PUBLIC_HOST_WITH_PORT')
WEBCLIENT_PUBLIC_HOST_WITH_PORT_WITH_PATH = os.environ.get('WEBCLIENT_PUBLIC_HOST_WITH_PORT_WITH_PATH')