from rest_framework import viewsets, filters, status
from rest_framework.decorators import action
from rest_framework.response import Response
from django.core.exceptions import PermissionDenied
from django_filters import rest_framework as django_rest_filters
from rest_framework.permissions import IsAuthenticated
from .permissions import IsOwnerOrReadOnly

from .models import ClassRoom
from .serializers import ClassRoomSerializer


class ClassRoomViewSet(viewsets.ModelViewSet):
    """
    ClassRoom viewset
    """
    queryset = ClassRoom.objects.order_by('-updated_at')
    serializer_class = ClassRoomSerializer
    permission_classrooms = (IsAuthenticated, IsOwnerOrReadOnly, )
    filter_backends = (filters.SearchFilter,
                       django_rest_filters.DjangoFilterBackend)
    search_fields = ('title', 'description', )
    filter_fields = ('owner_id', 'finished', )

    def get_queryset(self):
        if "id_in_members" in self.request.GET and self.request.GET["id_in_members"]:
            return self.queryset.filter(members__id__in=[self.request.GET["id_in_members"]])
        # elif self.request.user and not self.request.user.is_anonymous:
        #     return self.queryset.filter(members__in=[self.request.user])
        else:
            return self.queryset
            # raise PermissionDenied()

    def perform_create(self, serializer):
        """Add user that make request to serializer data"""
        if self.request.user and not self.request.user.is_anonymous:
            serializer.save(owner=self.request.user)
        else:
            raise PermissionDenied()

    @action(detail=True, methods=['post'], url_path='exit', permission_classrooms=[IsAuthenticated])
    def exit_classroom(self, request, pk=None):
        """Remove current user from a classroom"""
        if not self.request.user or self.request.user.is_anonymous:
            raise PermissionDenied()

        classroom = self.get_object()
        if not classroom:
            return Response(status=status.HTTP_404_NOT_FOUND)

        if self.request.user.id == classroom.owner_id:
            return Response({'detail': "You cannot exit a classroom you're owner of"}, status=status.HTTP_400_BAD_REQUEST)

        try:
            classroom.members.remove(self.request.user)
            return Response({'removed': True}, status=status.HTTP_200_OK)
        except:
            return Response({'detail': "Could not remove you from this classroom"}, status=status.HTTP_400_BAD_REQUEST)
