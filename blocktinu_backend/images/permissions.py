from rest_framework import permissions
from django.contrib.auth import get_user_model

User = get_user_model

from .models import Image


class IsAdminUserOrReadOnly(permissions.IsAdminUser):
    def has_permission(self, request, view):
        is_admin = super(
            IsAdminUserOrReadOnly,
            self).has_permission(request, view)
        return request.method in permissions.SAFE_METHODS or is_admin


class IsOwner(permissions.BasePermission):
    def has_permission(self, request, view):
        return True

    """
    Object-level permission to only allow owners of an object to edit it.
    Assumes the model instance has an `owner` attribute.
    """
    def has_object_permission(self, request, view, obj):
        # Read permissions are allowed to any request,
        # so we'll always allow GET, HEAD or OPTIONS requests.
        if isinstance(obj, User):
            # only user can edit himself
            if obj == request.user:
                return True
            else:
                return False

        if isinstance(obj, Image):
            owner = getattr(obj.content_object, 'owner', None)
            if owner and not owner.is_anonymous:
                return owner.id == request.user.id

        # no.
        return False


class IsOwnerOrReadOnly(IsOwner):
    def has_object_permission(self, request, view, obj):
        if request.method in permissions.SAFE_METHODS:
            return True

        return super(IsOwnerOrReadOnly, self).has_object_permission(request, view, obj)

