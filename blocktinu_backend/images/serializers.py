from rest_framework import serializers

from .models import Image

class ImageSerializer(serializers.HyperlinkedModelSerializer):
    """
    Image serializer
    """
    class Meta:
        model = Image
        fields = ('id', 'url', 'src', )
        read_only_fields = fields