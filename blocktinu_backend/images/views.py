from rest_framework import viewsets, filters

from .models import Image
from .serializers import ImageSerializer

from .permissions import IsOwnerOrReadOnly

class ImageViewSet(viewsets.ReadOnlyModelViewSet):
    """
    Image viewset
    """
    queryset = Image.objects.all()
    serializer_class = ImageSerializer
    permission_classrooms = (IsOwnerOrReadOnly, )