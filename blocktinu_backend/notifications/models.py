from django.db import models
from django.utils.timezone import now
from django.contrib.auth import get_user_model

User = get_user_model()

class Notification(models.Model):
    """
    Notification model
    """
    owner = models.ForeignKey(
        User, on_delete=models.SET_NULL, related_name='outgoing_notifications', null=True)
    recipient = models.ForeignKey(
        User, on_delete=models.CASCADE, related_name='notifications')

    title = models.TextField(max_length=30)
    text = models.TextField(max_length=150)
    notification_datetime = models.DateTimeField(default=now, blank=False)
    notified = models.BooleanField(default=False)
    redirect_path = models.TextField(max_length=20, blank=True)
    type = models.TextField(max_length=30, default="info")

    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def save(self, *args, **kwargs):
        """override save"""
        # set notified to false on instance update
        if 'update_fields' not in kwargs or 'notified' not in kwargs['update_fields']:
            self.notified = False
        super(Notification, self).save(*args, **kwargs)