"""Serializers"""
from django.db.models.signals import post_save
from django.dispatch import receiver
from rest_framework import serializers

from asgiref.sync import async_to_sync
from blocktinu_backend.socket_connection.utils import notify_user

from .models import Notification


class NotificationSerializer(serializers.HyperlinkedModelSerializer):
    """
    Notification Serializer
    """
    class Meta:
        model = Notification
        fields = ('id', 'url', 'recipient', 'recipient_id', 'owner', 'owner_id',
                  'title', 'type', 'text', 'notification_datetime',
                  'notified', 'redirect_path', 'created_at', 'updated_at')
        read_only_fields = ('id', 'url', 'notified')
        required_fields = ('recipient', 'title', 'text', )
        extra_kwargs = {field: {'required': True} for field in required_fields}


@receiver(post_save, sender=Notification, dispatch_uid='notification_post_save_signal')
def send_new_notification(sender, instance, created, **kwargs):
    """Send notification"""
    def send_notification(notification):
        if not notification or not getattr(notification, 'recipient', False):
            return False
        serializer = NotificationSerializer(
            notification, many=False, context={'request': None})
        async_to_sync(notify_user)(notification.recipient.id, serializer.data)

    if created:
        send_notification(instance)

