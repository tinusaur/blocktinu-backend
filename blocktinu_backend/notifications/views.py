from django_filters.rest_framework import DjangoFilterBackend
from rest_framework import filters, viewsets
from rest_framework.permissions import IsAuthenticated
from django.db.models import Q

from .models import Notification
from .permissions import IsOwnerOrReadOnly
from .serializers import NotificationSerializer


class NotificationViewSet(viewsets.ModelViewSet):
    """
    Notifications Viewset
    """
    queryset = Notification.objects.order_by('-created_at')
    serializer_class = NotificationSerializer
    permission_classrooms = (IsAuthenticated, IsOwnerOrReadOnly, )
    filter_backends = (filters.SearchFilter, DjangoFilterBackend, )
    filter_fields = ('recipient_id', 'owner_id', 'notified', )

    def perform_create(self, serializer):
        """Add user that make request to serializer data"""
        if self.request.user and not self.request.user.is_anonymous:
            serializer.save(owner=self.request.user)
        else:
            raise PermissionDenied()

    # read only
    def get_queryset(self):
        return self.queryset.filter(Q(recipient=self.request.user) | Q(owner=self.request.user))

