from blocktinu_backend.classrooms.models import ClassRoom
from django.db import models
from django.contrib.contenttypes.fields import GenericRelation
from django.contrib.auth import get_user_model
from blocktinu_backend.tags.models import Tag
from blocktinu_backend.images.models import Image
from django.contrib.postgres.fields import ArrayField

from django.db.models.signals import post_save
from django.dispatch import receiver

User = get_user_model()


class Project(models.Model):
    owner = models.ForeignKey(User, on_delete=models.SET_NULL, null=True)
    classroom = models.ForeignKey(ClassRoom, on_delete=models.SET_NULL, null=True)
    tags = models.ManyToManyField(Tag, related_name='projects')
    title = models.TextField(max_length=100, blank=False, null=False)
    description = models.TextField(max_length=900, blank=True, null=False)
    is_private = models.BooleanField(default=False)
    finished = models.BooleanField(default=False)
    autosave = models.BooleanField(default=False)

    BLOCK = "block"
    CODE = "code"
    TYPE_CHOICES = (
        (BLOCK, "block"),
        (CODE, "bode"),
    )
    type = models.CharField(
        max_length=5, choices=TYPE_CHOICES, null=False, blank=False)

    images = GenericRelation(Image)
    libraries = ArrayField(models.CharField(
        max_length=50, blank=False), blank=True, null=True)
    shield = models.CharField(max_length=50, blank=False, null=False)

    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def get_last_code_instance(self):
        return self.codes.order_by('-created_at').first()

class Code(models.Model):
    version = models.CharField(max_length=5)
    source = models.TextField(default="")
    project = models.ForeignKey(
        Project, related_name='codes', on_delete=models.CASCADE)
    created_at = models.DateTimeField(auto_now_add=True)