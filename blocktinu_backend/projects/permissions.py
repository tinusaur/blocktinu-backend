from rest_framework import permissions

from .models import Project


class IsOwner(permissions.BasePermission):
    """Is owner of project"""
    def has_permission(self, request, view):
        return True

    def has_object_permission(self, request, view, obj):

        if isinstance(obj, Project):
            return obj.owner == request.user
        # no.
        return False


class IsOwnerOrReadOnly(IsOwner):
    """Is project owner or read only"""
    def has_object_permission(self, request, view, obj):
        if request.method in permissions.SAFE_METHODS:
            return True

        return super(IsOwnerOrReadOnly, self).has_object_permission(request, view, obj)

