from rest_framework import serializers
from .models import Code, Project
from blocktinu_backend.images.models import Image
from blocktinu_backend.tags.models import Tag
from blocktinu_backend.images.serializers import ImageSerializer
from blocktinu_backend.tags.serializers import serialize_tag

from django.core.exceptions import ObjectDoesNotExist
from django.utils.encoding import smart_text

DEFAULT_BLOCK_CODE = '<xml xmlns="https://developers.google.com/blockly/xml"/>'

class CreatableSlugRelatedField(serializers.SlugRelatedField):
    def to_internal_value(self, data):
        try:
            return self.get_queryset().get_or_create(**{self.slug_field: data})[0]
        except ObjectDoesNotExist:
            self.fail('does_not_exist', slug_name=self.slug_field,
                      value=smart_text(data))
        except (TypeError, ValueError):
            self.fail('invalid')


class CodeSerializer(serializers.ModelSerializer):
    """Code serializer"""
    class Meta:
        model = Code
        fields = ("id", "version", "source", )
        

class ProjectSerializer(serializers.HyperlinkedModelSerializer):
    """Project serializer"""

    tags = CreatableSlugRelatedField(
        many=True,
        queryset=Tag.objects,
        slug_field="name"
    )

    images = ImageSerializer(
        many=True,
        read_only=True)

    class Meta:
        model = Project
        fields = ("id", "url", "owner", "owner_id", "classroom", "classroom_id", "title", "description", "is_private",
                  "type", "images", "finished", "autosave", "tags", "libraries", "shield", "created_at", "updated_at", )
        read_only_fields = ("id", "url", "owner", "images",
                            "created_at", "updated_at", )
        required_fields = ("title", "description", "type", "tags", "shield")
        extra_kwargs = {field: {"allow_null": False, "required": True}
                        for field in required_fields}

    def get_current_user(self):
        """Gets Current user from request"""
        user = None
        request = self.context.get("request")
        if request and hasattr(request, "user"):
            user = request.user
            return user
        return None

    def validate_owner(self, value):
        """Validate owner field"""
        user = self.get_current_user()
        if user.is_anonymous or user != value:
            raise serializers.ValidationError(
                ("You can not create a project for another user"))
        return value

    def create(self, validated_data):
        """Add image objects"""
        """Check count of image objects to create (allowed 1)"""
        image_data = self.context.get("view").request.FILES
        if len(image_data) > 1:
            raise serializers.ValidationError(
                ("You can not update more than one image!"))
        project_data = super().create(validated_data)
        for img in image_data.values():
            img = Image.objects.create(content_object=project_data, src=img)

        # create a code instance for new project
        code = self.context.get("view").request.data.get('code')
        if not code:
            code = DEFAULT_BLOCK_CODE if project_data.type == Project.BLOCK else ''
        Code.objects.create(project=project_data, source=code)

        return project_data

    def update(self, instance, validated_data):
        """Add image objects while companie object is on update"""
        """Check count of image objects to create (allowed 1)"""
        image_data = self.context.get("view").request.FILES
        if len(image_data) > 1:
            raise serializers.ValidationError(
                ("You can not update more than one image!"))
        # update
        project_data = super().update(instance, validated_data)
        # remove images
        if len(image_data) == 1:
            images_to_replace = Image.objects.filter(object_id=instance.id)
            images_to_replace.delete()

            for img in image_data.values():
                img = Image.objects.create(
                    content_object=project_data, src=img)
        return project_data

    def to_representation(self, instance):
        """Add tags serialized data to response"""
        response = super().to_representation(instance)
        if self.context["request"]:
            response["tags"] = serialize_tag(tags=instance.tags, many=True)
            # TODO: optimize?
            if instance.owner:
                response["owner_name"] = instance.owner.full_name
            if instance.classroom:
                response["classroom_name"] = "{} / {}".format(instance.classroom.owner.full_name, instance.classroom.title)
            if not isinstance(self.instance, list):
                code = self.instance.get_last_code_instance()
                if code:
                    response['code'] = code.source
        return response
