"""
Projects urls
"""

from rest_framework import routers
from django.conf.urls import url, include
from . import views

ROUTER = routers.DefaultRouter()
ROUTER.register(r'projects', views.ProjectViewSet, base_name="project")

urlpatterns = [
    url(r'', include(ROUTER.urls))
]

