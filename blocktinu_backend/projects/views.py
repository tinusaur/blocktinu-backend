from rest_framework import viewsets, filters, status
from rest_framework.decorators import action
from rest_framework.response import Response
from rest_framework.permissions import IsAuthenticated
from .permissions import IsOwnerOrReadOnly
from django.core.exceptions import PermissionDenied
from django_filters import rest_framework as django_rest_filters

from .models import Project
from .serializers import ProjectSerializer
from blocktinu_backend.tags.models import Tag


class ProjectFilter(django_rest_filters.FilterSet):
    """Custom filter for projects"""
    class Meta:
        model = Project
        fields = ["owner_id", "tags", "classroom_id"]

    tags = django_rest_filters.ModelMultipleChoiceFilter(
        queryset=Tag.objects,
        to_field_name='name',
        conjoined=True,
        method='filter_tags'
    )

    def filter_tags(self, queryset, name, tags):
        if tags:
            q = queryset.filter(tags__in=tags).distinct()
            return q
        else:
            return queryset


class ProjectViewSet(viewsets.ModelViewSet):
    """
    Projects Viewset
    """
    queryset = Project.objects.order_by('-updated_at')
    serializer_class = ProjectSerializer
    permission_classrooms = (IsOwnerOrReadOnly, )
    filter_backends = (filters.SearchFilter,
                       django_rest_filters.DjangoFilterBackend, )
    search_fields = ["title", "description", ]
    filter_class = ProjectFilter

    def perform_create(self, serializer):
        """Add user that make request to serializer data"""
        if self.request.user and not self.request.user.is_anonymous:
            serializer.save(owner=self.request.user)
        else:
            raise PermissionDenied()

    @action(detail=True, methods=['patch'], url_path='update_code', permission_classrooms=[IsAuthenticated, IsOwnerOrReadOnly])
    def update_code(self, request, pk=None):
        """Update project code"""
        if not self.request.user or self.request.user.is_anonymous:
            raise PermissionDenied()

        project = self.get_object()
        if not project:
            return Response(status=status.HTTP_404_NOT_FOUND)

        if self.request.user.id != project.owner_id:
            raise PermissionDenied()

        new_code_source = request.data['code']
        if not new_code_source:
            return Response({'code': 'This field is required'}, status=status.HTTP_400_BAD_REQUEST)
            
        try:
            code = project.get_last_code_instance()
            code.source = new_code_source
            code.save(update_fields=['source'])
            return Response({'updated': True}, status=status.HTTP_200_OK)
        except:
            return Response({'detail': "Failed to update code"}, status=status.HTTP_400_BAD_REQUEST)
