from channels.generic.websocket import AsyncJsonWebsocketConsumer
from django.utils import timezone
from channels.db import database_sync_to_async
from channels.layers import get_channel_layer

from blocktinu_backend.notifications.models import Notification
from blocktinu_backend.projects.models import Project
from .utils import get_room_name_for_project_id, get_room_name_for_user_id, broadcast_project_code_change


class GlobalConsumer(AsyncJsonWebsocketConsumer):
    # user = .....
    # current user token
    user_token = None
    # room
    room_project_name = None
    # each user has it's own room
    # so we can send notifications directly to this user.
    # (e.g. 'user_{user_id}')
    user_room_project = None
    # current project (one room = one project)
    project = None

    @database_sync_to_async
    def change_user_online_status(self, is_online):
        if not self.user.is_anonymous:
            cache_value = self.user.set_online_status(is_online)
            if not cache_value:
               # update last_login
                self.user.last_login = timezone.now()
                self.user.save(update_fields=['last_login'])

    @database_sync_to_async
    def get_project_by_id(self, id):
        try:
            return Project.objects.get(id=id)
        except:
            return None

    @database_sync_to_async
    def get_notification(self, id):
        try:
            return Notification.objects.get(id=id)
        except:
            return None

    @database_sync_to_async
    def set_notification_notified(self, id):
        try:
            result = Notification.objects.get(id=id)
            result.notified = True
            result.save(update_fields=['notified'])
            return result
        except:
            return None

    async def connect(self):
        self.user = self.scope["user"]

        if "token" in self.scope:
            self.user_token = self.scope["token"]

        print(self.user, self.user_token)
        # auth guard
        if not self.user or self.user.is_anonymous:
            print('close on auth guard')
            await self.close()
        else:
            try:
                protocol = self.scope['subprotocols'][0]
                # set a room name for current user
                self.user_room_project = get_room_name_for_user_id(
                    self.user.id)
                # add room
                await self.channel_layer.group_add(
                    self.user_room_project,
                    self.channel_name
                )
                print('connected to room {}, {}'.format(
                    self.user_room_project, self.channel_name))
                # accept
                await self.accept(protocol)
                # notify user that he's connected successfully
                await self.notify_using_channel_layer({
                    "type": "connected",
                    "payload": None
                })
                await self.change_user_online_status(True)
            except:
                print('close on except')
                # something went wrong, close connection
                await self.close()

    async def join_room(self, room_name):
        await self.leave_room()
        # Join project room
        print('join project room {}'.format(room_name))
        self.room_name = room_name

        self.project = await self.get_project_by_id(self.room_name)

        # check if projects exists and current user is a project member
        if not self.project or not self.project.classroom or not self.project.classroom.members.filter(id=self.user.id).exists():
            # denied or no classroom. do nothing.
            pass
        else:
            # accept
            # set project room name
            self.room_project_name = get_room_name_for_project_id(
                self.room_name)
            # add channel to project
            await self.channel_layer.group_add(
                self.room_project_name,
                self.channel_name
            )
            # print('connected to room {}'.format(self.room_project_name))
            # send joined_room socket msg
            content = {
                "type": "joined_room",
                "payload": {
                    "room_name": room_name
                },
            }
            await self.notify_using_channel_layer(content)

    async def receive_json(self, content, **kwargs):
        """
        This handles data sent over the wire from the client.

        We need to validate that the received data is of the correct
        form. You can do this with a simple DRF serializer.

        We then need to use that validated data to confirm that the
        global/ng user (available in self.scope["user"] because of
        the use of channels.auth.AuthMiddlewareStack in routing) is
        allowed to subscribe to the requested object.
        """
        if content['type'] == 'join_room_request':
            # join room
            payload = content["payload"]
            room_name = payload["room_name"]
            await self.join_room(room_name)
        elif content['type'] == 'leave_room':
            # leave room
            await self.leave_room()
        elif content['type'] == 'broadcast_code_change':
            # broadcast code change
            payload = content["payload"]
            await broadcast_project_code_change(self.room_project_name, payload)
        elif content['type'] == 'notification_ack':
            # client says that he is acknowleged about notification
            payload = content["payload"]
            await self.notification_ack(payload)

    # client says that he is acknowleged about notification
    async def notification_ack(self, payload):
        notification_id = payload["notification_id"]
        if notification_id:
            await self.set_notification_notified(notification_id)

    # disconnect
    async def disconnect(self, close_code):
        await self.change_user_online_status(False)
        await self.leave_room()

    # leave_room
    async def leave_room(self):
        # Leave project room
        if self.room_project_name and self.channel_name:
            await self.channel_layer.group_discard(
                self.room_project_name,
                self.channel_name
            )

    # helper, notifies using current channel layer
    async def notify_using_channel_layer(self, content):
        await self.channel_layer.send(self.channel_name, {
            "type": "notify",
            "content": content,
        })

    async def notify(self, event):
        """
        This handles calls elsewhere in this codebase that look
        like:

            channel_layer.group_send(group_name, {
                'type': 'notify',  # This routes it to this handler.
                'content': json_message,
            })

        Don't try to directly use send_json or anything; this
        decoupling will help you as things grow.
        """
        await self.send_json(event["content"])
