from channels.generic.websocket import AsyncJsonWebsocketConsumer
from channels.db import database_sync_to_async
from channels.layers import get_channel_layer

from django.urls import resolve

from blocktinu_backend.notifications.models import Notification
from django.core.cache import cache

from django.utils.translation import ugettext as _


#
# Room names
#

def get_room_name_for_project_id(room_name):
    return 'project_%s' % room_name


def get_room_name_for_user_id(user_id):
    return 'user_%s' % user_id


#
# Db async
#

#
# Broadcast project code change
#
async def broadcast_project_code_change(project_room, payload):
    if project_room:
        group_name = project_room
        channel_layer = get_channel_layer()
        content = {
            "type": "project_code_change",
            "payload": payload,
        }
        await channel_layer.group_send(group_name, {
            "type": "notify",
            "content": content,
        })


async def send_group_notification(group_name, notification):
    print('notify {} with n#{}'.format(group_name, notification))
    channel_layer = get_channel_layer()
    content = {
        "type": "notification",
        "payload": notification,
    }
    await channel_layer.group_send(group_name, {
        "type": "notify",
        "content": content,
    })


async def notify_user(user_id, notification_serializer_data):
    channel_layer = get_channel_layer()
    await send_group_notification(get_room_name_for_user_id(user_id), notification_serializer_data)
