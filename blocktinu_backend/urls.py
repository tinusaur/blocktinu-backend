"""blocktinu_backend URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from django.conf.urls import url, include

from django.conf.urls.static import static
from django.conf import settings

urlpatterns = [
    path('admin/', admin.site.urls),
    url(r'^api/auth/', include('djoser.urls')),
    url(r'^api/auth/', include('djoser.urls.authtoken')),
    url(r"^api/auth/", include("djoser.social.urls")),
    url(r'^api/auth/', include('rest_social_auth.urls_token')),
    url(r'^api/', include('blocktinu_backend.tags.urls')),
    url(r'^api/', include('blocktinu_backend.images.urls')),
    url(r'^api/', include('blocktinu_backend.classrooms.urls')),
    url(r'^api/', include('blocktinu_backend.projects.urls')),
    url(r'^api/', include('blocktinu_backend.notifications.urls')),
    url(r'^api/', include('blocktinu_backend.authentication.urls')),
]

# Serve media in DEBUG = True mode
urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
