#!/bin/bash

pip install -r requirements.txt

#python manage.py collectstatic
python manage.py makemigrations authentication images tags classrooms projects notifications socket_connection
python manage.py migrate

if [ "$ENV" = "production" ]; then
    echo Production env
    #python manage.py runserver "0.0.0.0:${INTERNAL_PORT}"
    export DJANGO_SETTINGS_MODULE=blocktinu_backend.settings

    daphne -b 0.0.0.0 -p ${INTERNAL_PORT} blocktinu_backend.asgi:application
else
    echo Developement env
    python manage.py runserver "0.0.0.0:${INTERNAL_PORT}"
fi

